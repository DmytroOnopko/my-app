export const databaseTypes = {
    GET_DATA: "DATABASE/GET_DATA"
}

export const databaseActions = {

    getAuthors: queries => ({
        type: databaseTypes.GET_DATA,
        payload: queries
    })

}