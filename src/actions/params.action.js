export const paramsTypes = {

    SET_PARAMS: "PARAMS/SET_PARAMS",
    SET_PARAM_SORT: "PARAMS/SET_PARAM_SORT",
    SET_PARAM_COUNT: "PARAMS/SET_PARAM_COUNT",
    SET_PARAM_KEYWORD: "PARAMS/SET_PARAM_KEYWORD"

};

export const paramsActions = {

    setParamSort: sort => ({
        type: paramsTypes.SET_PARAM_SORT,
        payload: sort
    }),

    setParamCount: count => ({
        type: paramsTypes.SET_PARAM_COUNT,
        payload: count
    }),

    setParamKeyword: keyword => ({
        type: paramsTypes.SET_PARAM_KEYWORD,
        payload: keyword
    }),

    setParams: params => ({
        type: paramsTypes.SET_PARAMS,
        payload: params
    })

}