export const sortTypes = {
    NAME_DESC: "NAME_DESC",
    NAME_ASC: "NAME_ASC",
    VIEWS_DESC: "VIEWS_DESC",
    VIEWS_ASC: "VIEWS_ASC",
}