import React from 'react';

const ArrowLeftIcon = ({onClick}) =>
    <svg onClick={onClick}
         xmlns="http://www.w3.org/2000/svg" width="10" height="15" viewBox="0 0 8 13" fill="none">
        <path
            d="M7.70504 2.11502L6.29504 0.705017L0.295044 6.70502L6.29504 12.705L7.70504 11.295L3.12504 6.70502L7.70504 2.11502Z"
            fill="#ffffff70"/>
    </svg>;

export default ArrowLeftIcon;