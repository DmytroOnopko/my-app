import React from 'react';

const ArrowRightIcon = ({onClick}) =>
    <svg onClick={onClick} xmlns="http://www.w3.org/2000/svg" width="10" height="15" viewBox="0 0 8 13" fill="none">
        <path d="M0.295017 11.295L1.70502 12.705L7.70502 6.70502L1.70502 0.705017L0.295017 2.11502L4.87502 6.70502L0.295017 11.295Z" fill="#ffffff70"/>
    </svg>;

export default ArrowRightIcon;