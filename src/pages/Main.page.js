import React from "react";
import styleMain from "./Main.module.scss";
import AuthorsListContainer from "../containers/AuthorsList.container";
import PaginationContainer from "../containers/Pagination.container";
import SearchContainer from "../containers/SearchContainer";
import SortContainer from "../containers/Sort.container";

const MainPage = () => {

    return (
        <div className={styleMain.container}>
            <div className={styleMain.holder}>
                <SearchContainer/>
                <SortContainer/>
                <AuthorsListContainer/>
            </div>
            <PaginationContainer/>
        </div>
    )
};

export default MainPage;