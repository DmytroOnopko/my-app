export const getParamCount = state => state.count;
export const getParamSort = state => state.sort;
export const getParamKeyword = state => state.keyword;
export const getParams = state => ({
    count: state.count,
    sort: state.sort,
    keyword: state.keyword
})