export const getAuthors = state => state.authors;
export const getTotalAuthors = state => state.totalAuthors;
export const getTopThreeAuthors = state => state.topThreeAuthors;