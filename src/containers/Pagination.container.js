import React from "react";
import Pagination from "../components/Pagination/Pagination";
import {connect} from "react-redux";
import {getTotalAuthors} from "../selectors/database.selectors";
import {paramsActions} from "../actions/params.action";
import {getParamCount} from "../selectors/params.selector";

const PaginationContainer = ({totalAuthors, setParamCount, count}) => {

    return <Pagination totalAuthors={totalAuthors}
                       setParamCount={setParamCount}
                       count={count}/>
};

export default connect(
    ({databaseReducer, paramsReducer}) => ({
        totalAuthors: getTotalAuthors(databaseReducer),
        count: getParamCount(paramsReducer)
    }),
    {
        setParamCount: paramsActions.setParamCount
    }
)(PaginationContainer);