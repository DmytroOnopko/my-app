import React from "react";
import Sort from "../components/Sort/Sort";
import {connect} from "react-redux";
import {paramsActions} from "../actions/params.action";

const SortContainer = ({setParamSort}) => {

    return <Sort setParamSort={setParamSort}/>;
};

export default connect(
    null,
    {
        setParamSort: paramsActions.setParamSort,
    }
)(SortContainer);