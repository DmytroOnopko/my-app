import React from "react";
import Search from "../components/Search/Search";
import {connect} from "react-redux";
import {paramsActions} from "../actions/params.action";

const SearchContainer = ({setParams}) => {

    return <Search setParams={setParams}/>;
};

export default connect(
    null,
    {
        setParams: paramsActions.setParams,
    }
)(SearchContainer);