import React from "react";
import useDeepCompareEffect from "use-deep-compare-effect";
import AuthorsList from "../components/AuthorsList/AuthorsList";
import {connect} from "react-redux";
import {getAuthors, getTopThreeAuthors} from "../selectors/database.selectors";
import {databaseActions} from "../actions/database.action";
import {getParams} from "../selectors/params.selector";
//test

const AuthorsListContainer = ({authors, getAuthors, topThreeAuthors, params}) => {

    useDeepCompareEffect(() => {
        getAuthors(params);
    }, [getAuthors, params]);

    return <AuthorsList authors={authors}
                        topThreeAuthors={topThreeAuthors}
                        count={params.count}/>


};

export default connect(
    ({databaseReducer, paramsReducer}) => ({
        authors: getAuthors(databaseReducer),
        topThreeAuthors: getTopThreeAuthors(databaseReducer),
        params: getParams(paramsReducer)
    }),
    {
        getAuthors: databaseActions.getAuthors,
    }
)(AuthorsListContainer);