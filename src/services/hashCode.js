export const hashCode = (str) => {
    try {
        return Math.abs(
            str.split("")
                .reduce((a, b) => {
                    a = ((a << 5) - a) + b.charCodeAt(0);
                    return a & a;
                }, 0));
    } catch (e) {
        return undefined
    }
};