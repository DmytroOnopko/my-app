import {sortTypes} from "../constants/sort.constant";

export const SortService = (queries, data) => {
    let result = [...data];
    let total = data.length;
    try {
        const {sort, keyword, count = [0, 10]} = queries;
        if (sort){
            if (sort === sortTypes.VIEWS_DESC){
                result = result.sort((a, b) => b["pageviews"] - a["pageviews"]);
            }
            if (sort === sortTypes.VIEWS_ASC){
                result = result.sort((a, b) => a["pageviews"] - b["pageviews"]);
            }
            if (sort === sortTypes.NAME_DESC) {
                result = result.sort((a, b) => a["name"].localeCompare(b["name"]));
            }
            if (sort === sortTypes.NAME_ASC) {
                result = result.sort((a, b) => b["name"].localeCompare(a["name"]))
            }
        }
        if (keyword) {
            let startedWord = new RegExp(`^${keyword}`, "i");
            let wordAfterSpace = new RegExp(`\\s+${keyword}`, "i");
            result = result.filter( author => startedWord.test(author.name) || wordAfterSpace.test(author.name));
            total = result.length;
        }
        if (count) {
            result = result.slice(count[0], count[1])
        }
        return {result, total};
    } catch (e) {
        return {result: data.slice(0, 10), total}
    }
}