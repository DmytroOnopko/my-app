import React from "react";
import styleSub from "./Sub.module.scss";

const SubField = ({subFieldText}) =>
    <p className={styleSub.sub}>{subFieldText}</p>;

export default SubField;