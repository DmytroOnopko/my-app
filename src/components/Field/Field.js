import React from "react";
import styleField from "./Field.module.scss";

const Field = ({fieldText}) => {

    return <h4 className={styleField.field}>{fieldText}</h4>
};

export default Field;