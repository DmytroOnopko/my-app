import React from "react";
import Author from "../Author/Author";
import SubField from "../Field/Sub/Sub.field";
import styleList from "./List.module.scss";

const AuthorsList = ({authors, topThreeAuthors, count}) => {

    const result = count?.[0] || 0;
    return (
        <div className={styleList.list}>
            {authors.map((author, index) => {
                const indexIcon = topThreeAuthors.findIndex(item => item["pageviews"] === author["pageviews"]);

                return <Author key={author.name}
                               author={author}
                               index={index + result}
                               indexIcon={indexIcon}/>
            })}
            {!authors.length &&
                <div className={styleList.empty}>
                    <SubField subFieldText={"Список пуст"}/>
                </div>}
        </div>
    )
};

export default AuthorsList;