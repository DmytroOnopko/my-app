import React, {useState, useCallback} from "react";
import styleSort from "./Sort.module.scss";
import SubField from "../Field/Sub/Sub.field";
import SortIcon from "../../ui/Icons/Sort.icon";
import {sortTypes} from "../../constants/sort.constant";
import debounce from "lodash.debounce";
const MS = 300;

const Sort = ({setParamSort}) => {

    const [sort, setSort] = useState(sortTypes.VIEWS_DESC);
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const delayed = useCallback(debounce(sort => {
        setParamSort(sort);
    }, MS), []);

    const handleSortByName = useCallback(() => {
        const result = sort === sortTypes.NAME_DESC ? sortTypes.NAME_ASC : sortTypes.NAME_DESC
        setSort(result);
        delayed(result);
    }, [delayed, sort]);

    const handleSortByViews = useCallback(() => {
        const result = sort === sortTypes.VIEWS_DESC ? sortTypes.VIEWS_ASC : sortTypes.VIEWS_DESC
        setSort(result);
        delayed(result);
    }, [delayed, sort]);

    const styleName = `
        ${styleSort.name}
        ${sort === sortTypes.NAME_DESC ? styleSort["name--DESC"] : styleSort["name--ASC"]}
        ${sort !== sortTypes.NAME_ASC && sort !== sortTypes.NAME_DESC ? styleSort["name--hide"] : ""}
    `;

    const styleViews = `
        ${styleSort.views}
        ${sort === sortTypes.VIEWS_DESC ? styleSort["views--DESC"] : styleSort["views--ASC"]}
        ${sort !== sortTypes.VIEWS_ASC && sort !== sortTypes.VIEWS_DESC ? styleSort["views--hide"] : ""}
    `;

    return (
        <div className={styleSort.sort}>
            <div className={styleName}
                 onClick={handleSortByName}>
                <SubField subFieldText={"имя фамилия"}/>
                <SortIcon/>
            </div>
            <div className={styleViews}
                 onClick={handleSortByViews}>
                <SortIcon/>
                <SubField subFieldText={"кол-во просмотров"}/>
            </div>
        </div>
    )
};

export default Sort;