import React from "react";
import FirstIcon from "../ui/Icons/First/First.icon";
import SecondIcon from "../ui/Icons/Second/Second.icon";
import ThirdIcon from "../ui/Icons/Third/Third.icon";

const GetIcon = ({indexIcon}) => {

    const medals = [<FirstIcon/>, <SecondIcon/>, <ThirdIcon/>];

    return medals[indexIcon] || <div/>
};

export default GetIcon;