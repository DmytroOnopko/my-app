import React, {memo} from "react";
import SubField from "../Field/Sub/Sub.field";
import Avatar from "../Avatar/Avatar";
import Field from "../Field/Field";
import Amount from "../Amount";
import styleAuthor from "./Author.module.scss";
import GetIcon from "../GetIcon";

const Author = ({author, index, indexIcon}) => {

    const style = `${styleAuthor.item} ${index % 2 !== 0 ? styleAuthor["item--mark"] : "''"}`;

    return (
        <div className={style}>
            <SubField subFieldText={index + 1}/>
            <Avatar name={author.name}/>
            <div className={styleAuthor.title}>
                <Field fieldText={author.name}/>
                <SubField subFieldText={author["count_pub"].toString().concat(" публ.")}/>
            </div>
            <GetIcon indexIcon={indexIcon}/>
            <Amount amount={author["pageviews"]}/>
        </div>
    )
};

export default memo(Author);