import React from "react";
import {hashCode} from "../../services/hashCode";
import styleAvatar from "./Avatar.module.scss";
import colors from "../../colors/colors.json";

const Avatar = ({name}) => {

    const initials = name.split(' ').reduce((accum, item) => accum.concat(item[0]), '');
    const index = hashCode(name) % colors.length;
    const color = {backgroundColor: colors[index]};

    return (
        <div className={styleAvatar.avatar} style={color}>
            <div className={styleAvatar.name}>
                {initials}
            </div>
        </div>
    )
};

export default Avatar;