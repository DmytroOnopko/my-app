import React from "react";
import Field from "./Field/Field";

const Amount = ({amount}) => {
    const result = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    return <Field fieldText={result}/>
};

export default Amount;