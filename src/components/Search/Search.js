import React, {useState, useCallback} from "react";
import ZoomIcon from "../../ui/Icons/Zoom.icon";
import styleSearch from "./Search.module.scss";
import debounce from "lodash.debounce";
const MS = 300;

const Search = ({setParams}) => {

    const [keyword, setKeyword] = useState("");

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const delayed = useCallback(debounce(key => {
        setParams({
            keyword: key,
            count: [0, 10],
        });
    }, MS), []);

    const handleKeyword = useCallback(e => {
        setKeyword(e.target.value);
        delayed(e.target.value);
    }, [delayed]);

    return (
        <div className={styleSearch.search}>
            <ZoomIcon/>
            <input className={styleSearch["search-input"]}
                   type="text"
                   placeholder={"Поиск авторов по имени"}
                   value={keyword}
                   onChange={handleKeyword}/>
        </div>
    )
};

export default Search;