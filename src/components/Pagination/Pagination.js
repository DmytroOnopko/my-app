import React, {useCallback} from "react";
import stylePagination from "./Pagination.module.scss";
import ArrowLeftIcon from "../../ui/Icons/ArrowLeft.icon";
import ArrowRightIcon from "../../ui/Icons/ArrowRight.icon";
import Field from "../Field/Field";
import debounce from "lodash.debounce";

const MS = 300;
const MAX_RESULT_COUNT = 10;

const Pagination = ({totalAuthors, setParamCount, count}) => {

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const delayed = useCallback(debounce(count => {
        setParamCount(count);
    }, MS), []);

    const handleNext = useCallback( () => {
        const result = [count[1], count[1] + MAX_RESULT_COUNT];
        delayed(result);
    },[delayed, count]);

    const handlePrev = useCallback(() => {
        const result = [count[0] - MAX_RESULT_COUNT, count[1] - MAX_RESULT_COUNT];
        delayed(result);
    }, [delayed, count]);

    const styleHolder = `
        ${stylePagination.holder} 
        ${count[0] === 0 ? stylePagination["holder--hide-left"] : ""}
        ${count[1] > totalAuthors  ? stylePagination["holder--hide-right"] : ""}
    `;

    return (
        <div className={stylePagination.container}>
            <div className={styleHolder}>
                <ArrowLeftIcon onClick={handlePrev}/>
                <Field fieldText={`${count[0] || 1} - ${count[1]}`}/>
                <ArrowRightIcon onClick={handleNext}/>
            </div>
        </div>
    )
};

export default Pagination;