import {paramsTypes} from "../actions/params.action";
import {sortTypes} from "../constants/sort.constant";

const INITIAL_STATE = {
    count: [0, 10],
    sort: sortTypes.VIEWS_DESC,
    keyword: ""
}

const paramsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case paramsTypes.SET_PARAMS:
            return {
                ...state,
                ...action.payload
            }

        case paramsTypes.SET_PARAM_COUNT:
            return {
                ...state,
                ...{count: action.payload}
            };

        case paramsTypes.SET_PARAM_SORT:
            return {
                ...state,
                ...{sort: action.payload}
            };

        case paramsTypes.SET_PARAM_KEYWORD:
            return {
                ...state,
                ...{keyword: action.payload}
            };

        default:
            return state;
    }
};

export default paramsReducer;