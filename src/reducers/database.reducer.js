import {databaseTypes} from "../actions/database.action";
import database from "../database/data.json";
import {SortService} from "../services/sort.service";

const authors = [...database];

const INITIAL_STATE = {
    authors: [],
    topThreeAuthors: authors.sort((a, b) => b["pageviews"] - a["pageviews"]).slice(0, 3),
    totalAuthors: authors.length,
}

const databaseReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case databaseTypes.GET_DATA:
            const {result, total} = SortService(action.payload, [...authors]);
            return {
                ...state,
                ...{
                    authors: result,
                    totalAuthors: total
                }
            }

        default:
            return state
    }
}

export default databaseReducer;