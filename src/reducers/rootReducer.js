import {combineReducers} from "redux";
import databaseReducer from "./database.reducer";
import paramsReducer from "./params.reducer";

export const rootReducer = combineReducers({
    paramsReducer,
    databaseReducer
})